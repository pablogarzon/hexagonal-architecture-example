package hexagonal.architecture.example.infra.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import hexagonal.architecture.example.infra.entities.TransferenciaEntity;

@Repository
public interface ITransferenciaRepository extends MongoRepository<TransferenciaEntity, Integer> {

	List<TransferenciaEntity> findByFechaGreaterThan(LocalDate fecha);
}
