package hexagonal.architecture.example.infra.adapters;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hexagonal.architecture.example.core.domain.Fecha;
import hexagonal.architecture.example.core.domain.Transferencia;
import hexagonal.architecture.example.core.repositories.TransferenciaRepository;
import hexagonal.architecture.example.infra.dao.ITransferenciaRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class TransferenciaRepositoryAdapter implements TransferenciaRepository {

	private final ITransferenciaRepository repository; 
	
	@Override
	public List<Transferencia> obtenerTransferenciasDesde(LocalDate from) {
		var transferencias = repository.findByFechaGreaterThan(from);
		return transferencias.stream()
				.map(t -> Transferencia.builder()
							.id(t.getId())
							.idEmpresa(t.getIdEmpresa())
							.importe(t.getImporte())
							.cuentaDebito(t.getCuentaDebito())
							.cuentaCredito(t.getCuentaCredito())
							.fecha(new Fecha(t.getFecha()))
							.build()
				)
				.collect(Collectors.toList());
	}

}
