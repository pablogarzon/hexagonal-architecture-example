package hexagonal.architecture.example.infra.entities;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Document(collection = "empresas")
public class EmpresaEntity {
	@Id
	private int cuit;
	private String razonSocial;
	private LocalDate fechaAdhesion;
}
