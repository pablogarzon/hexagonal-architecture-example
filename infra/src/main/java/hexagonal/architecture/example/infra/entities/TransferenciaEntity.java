package hexagonal.architecture.example.infra.entities;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Document(collection = "transferencias")
public class TransferenciaEntity {

	@Id
	private int id;
	private double importe;
	private int idEmpresa;
	private long cuentaDebito;
	private long cuentaCredito;
	private LocalDate fecha;
}
