package hexagonal.architecture.example.infra.adapters;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import hexagonal.architecture.example.core.domain.Empresa;
import hexagonal.architecture.example.core.domain.Fecha;
import hexagonal.architecture.example.core.repositories.EmpresaRepository;
import hexagonal.architecture.example.infra.dao.IEmpresaRepository;
import hexagonal.architecture.example.infra.entities.EmpresaEntity;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class EmpresaRepositoryAdapter implements EmpresaRepository {

	private final IEmpresaRepository repository;
	
	@Override
	public List<Empresa> obtenerEmpresas(Collection<Integer> empresasIds) {
		var empresas = repository.findAllById(empresasIds);
		return StreamSupport.stream(empresas.spliterator(), false)
				.map(e -> new Empresa(e.getCuit(), e.getRazonSocial(), new Fecha(e.getFechaAdhesion())))
				.collect(Collectors.toList());
	}

	@Override
	public List<Empresa> obtenerEmpresasAdheridasDesde(LocalDate fecha) {
		var empresas = repository.findByFechaAdhesionGreaterThan(fecha);
		return empresas.stream()
				.map(e -> new Empresa(e.getCuit(), e.getRazonSocial(), new Fecha(e.getFechaAdhesion())))
				.collect(Collectors.toList());
	}
	
	@Override
	public boolean existe(int cuit) {
		return repository.existsById(cuit);
	}

	@Override
	public boolean crear(Empresa empresa) {
		var empresaEntity = EmpresaEntity.builder()
				.cuit(empresa.getCuit())
				.razonSocial(empresa.getRazonSocial())
				.fechaAdhesion(empresa.getFechaAdhesion().getDate())
				.build();
				
		repository.save(empresaEntity);
		return true;
	}
}
