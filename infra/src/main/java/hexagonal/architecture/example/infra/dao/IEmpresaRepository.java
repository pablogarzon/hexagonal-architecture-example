package hexagonal.architecture.example.infra.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import hexagonal.architecture.example.infra.entities.EmpresaEntity;

@Repository
public interface IEmpresaRepository extends MongoRepository<EmpresaEntity, Integer> {
	
	List<EmpresaEntity> findByFechaAdhesionGreaterThan(LocalDate fechaAdhesion);

}
