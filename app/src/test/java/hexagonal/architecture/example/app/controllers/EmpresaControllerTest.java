package hexagonal.architecture.example.app.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import hexagonal.architecture.example.app.dto.CrearEmpresaRequest;
import hexagonal.architecture.example.app.dto.ObtenerEmpresasResponse;
import hexagonal.architecture.example.infra.dao.IEmpresaRepository;
import hexagonal.architecture.example.infra.dao.ITransferenciaRepository;
import hexagonal.architecture.example.infra.entities.EmpresaEntity;
import hexagonal.architecture.example.infra.entities.TransferenciaEntity;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
class EmpresaControllerTest {	

	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private IEmpresaRepository empresaRepository;
	
	@Autowired
	private ITransferenciaRepository transferenciaRepository;
	
	private List<EmpresaEntity> empresas; 
	
	private List<TransferenciaEntity> transferencias;
	
	private static final int EMPRESA_CON_TRANSF_RECIENTE = 100;
	
	@BeforeEach
	private  void setUp() {
		empresaRepository.deleteAll();
		
		var empresa1 = new EmpresaEntity(EMPRESA_CON_TRANSF_RECIENTE, "test1", LocalDate.now());
		var empresa2 = new EmpresaEntity(123, "test2", LocalDate.now().minusMonths(4));
		var empresa3 = new EmpresaEntity(321, "test3", LocalDate.now().minusDays(25));
		empresas = List.of(empresa1, empresa2, empresa3);
		
		var t1 = TransferenciaEntity.builder()
			.id(1)
			.idEmpresa(EMPRESA_CON_TRANSF_RECIENTE)
			.cuentaCredito(1234)
			.cuentaDebito(0304)
			.fecha(LocalDate.now())
			.importe(10)
			.build();
		transferencias = List.of(t1);
		
		empresaRepository.saveAll(empresas);
		transferenciaRepository.saveAll(transferencias);
	}
	
	@Test
	void givenEmpresasAdheridasUltimoMesURI_whenMockMVC_thenReturnsListOfEmpresas() throws Exception {		
		var request = MockMvcRequestBuilders.get("/empresas?adhesion=");
		MvcResult result = mvc.perform(request).andReturn();		
		String content = result.getResponse().getContentAsString();
		var response = new ObjectMapper().readValue(content, ObtenerEmpresasResponse.class);
		assertEquals(200, result.getResponse().getStatus());
		assertEquals(2, response.getEmpresas().size());
	}
	
	@Test
	void givenEmpresasConTransferenciasUltimoMesURI_whenMockMVC_thenReturnsListOfEmpresas() throws Exception {		
		var request = MockMvcRequestBuilders.get("/empresas?transferencias=");
		MvcResult result = mvc.perform(request).andReturn();		
		String content = result.getResponse().getContentAsString();
		var response = new ObjectMapper().readValue(content, ObtenerEmpresasResponse.class);
		assertEquals(200, result.getResponse().getStatus());
		assertEquals(1, response.getEmpresas().size());
	}
	
	@Test
	void givenValidEmpresa_WhenNewEmpresa_ThenReturnOk() throws Exception {
		var dto = new CrearEmpresaRequest(12, "test4");
		var request = MockMvcRequestBuilders.post("/empresas")
				 .content(new ObjectMapper().writeValueAsString(dto))
				 .contentType(MediaType.APPLICATION_JSON)
				 .accept(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(request).andReturn();
		assertEquals(201, result.getResponse().getStatus());
	}
	
	@Test
	void givenInvalidEmpresa_WhenNewEmpresa_ThenReturnConflict() throws Exception {
		var dto = new CrearEmpresaRequest(EMPRESA_CON_TRANSF_RECIENTE, "test4");
		var request = MockMvcRequestBuilders.post("/empresas")
				 .content(new ObjectMapper().writeValueAsString(dto))
				 .contentType(MediaType.APPLICATION_JSON)
				 .accept(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(request).andReturn();
		assertEquals(409, result.getResponse().getStatus());
	}
}

