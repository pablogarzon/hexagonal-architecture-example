package hexagonal.architecture.example.app.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hexagonal.architecture.example.app.dto.CrearEmpresaRequest;
import hexagonal.architecture.example.app.dto.ObtenerEmpresasResponse;
import hexagonal.architecture.example.core.application.CrearEmpresa;
import hexagonal.architecture.example.core.application.ObtenerEmpresasAdheridasUltimoMes;
import hexagonal.architecture.example.core.application.ObtenerEmpresasConTransferenciasUltimoMes;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class EmpresaController {

	private final ObtenerEmpresasConTransferenciasUltimoMes empresasConTranserfenciasService;
	private final ObtenerEmpresasAdheridasUltimoMes empresasAdheridasService;
	private final CrearEmpresa crearEmpresaService;

	@GetMapping(path = "/empresas", params = "transferencias")
	public ObtenerEmpresasResponse obtenerEmpresasConTransferencias(
			@RequestParam(defaultValue = "conTrasferenciasUltimoMes") String transferencias) {
		var empresas = empresasConTranserfenciasService.obtenerEmpresas();
		return ObtenerEmpresasResponse.fromDomain(empresas);
	}

	@GetMapping(path = "/empresas", params = "adhesion")
	public ObtenerEmpresasResponse obtenerUltimasEmpresas(
			@RequestParam(defaultValue = "adheridasUltimoMes") String adhesion) {
		var empresas = empresasAdheridasService.obtenerEmpresas();
		return ObtenerEmpresasResponse.fromDomain(empresas);
	}

	@PostMapping(path = "/empresas")
	public ResponseEntity<HttpStatus> crearEmpresa(@RequestBody CrearEmpresaRequest request) {
		if (crearEmpresaService.crear(request.toDomain())) {
			return new ResponseEntity<>(HttpStatus.CREATED);
		}
		return new ResponseEntity<>(HttpStatus.CONFLICT);
	}
}
