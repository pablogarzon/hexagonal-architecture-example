package hexagonal.architecture.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmpresaResponse {

	private int cuit;
	private String razonSocial;
	private String fechaAdhesion;
}
