package hexagonal.architecture.example.app.dto;

import hexagonal.architecture.example.core.domain.Empresa;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CrearEmpresaRequest {

	private int cuit;
	private String razonSocial;
	
	public Empresa toDomain() {
		return Empresa.builder()
				.cuit(cuit)
				.razonSocial(razonSocial)
				.build();
	}

}
