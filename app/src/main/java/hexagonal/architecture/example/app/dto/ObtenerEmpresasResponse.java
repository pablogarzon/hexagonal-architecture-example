package hexagonal.architecture.example.app.dto;

import java.util.List;
import java.util.stream.Collectors;

import hexagonal.architecture.example.core.domain.Empresa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ObtenerEmpresasResponse {

	private List<EmpresaResponse> empresas;
	
	public static ObtenerEmpresasResponse fromDomain(List<Empresa> empresas) {
		var list = empresas.stream()
				.map(c -> new EmpresaResponse(c.getCuit(), c.getRazonSocial(), c.getFechaAdhesion().getFormattedDate()))
				.collect(Collectors.toList());
		return new ObtenerEmpresasResponse(list);
	}
}
