package hexagonal.architecture.example.core.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import hexagonal.architecture.example.core.domain.Empresa;
import hexagonal.architecture.example.core.domain.Fecha;
import hexagonal.architecture.example.core.domain.Transferencia;
import hexagonal.architecture.example.core.repositories.EmpresaRepository;
import hexagonal.architecture.example.core.repositories.TransferenciaRepository;

class ObtenerEmpresasConTransferenciasUltimoMesTest {

	
	private TransferenciaRepository transferenciaRepository;
	private EmpresaRepository empresaRepository;

	@Test
	void testObtenerEmpresas() {
		
		transferenciaRepository = mock(TransferenciaRepository.class);
		empresaRepository = mock(EmpresaRepository.class);
		
		var t1 = Transferencia.builder()
				.id(1)
				.idEmpresa(001)
				.cuentaCredito(1234)
				.cuentaDebito(0304)
				.fecha(Fecha.crearFechaActual())
				.importe(10)
				.build();
		var t2 = Transferencia.builder()
				.id(2)
				.idEmpresa(002)
				.cuentaCredito(1234)
				.cuentaDebito(0304)
				.fecha(Fecha.crearFechaActual())
				.importe(15)
				.build();
		
		var transferencias = List.of(t1, t2);
		
		var empresa1 = new Empresa(001, "test1", Fecha.crearFechaActual());
		var empresa2 = new Empresa(002, "test2", Fecha.crearFechaActual());
		
		var empresas = List.of(empresa1, empresa2);
		
		when(transferenciaRepository.obtenerTransferenciasDesde(Mockito.any(LocalDate.class))).thenReturn(transferencias);
		when(empresaRepository.obtenerEmpresas(Mockito.anyCollection())).thenReturn(empresas);
		
		var uc = new ObtenerEmpresasConTransferenciasUltimoMes(transferenciaRepository, empresaRepository);
		var result = uc.obtenerEmpresas();
		assertEquals(empresas.size(), result.size());
	}

}
