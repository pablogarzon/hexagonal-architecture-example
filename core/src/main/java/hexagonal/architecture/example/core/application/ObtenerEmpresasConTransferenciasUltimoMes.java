package hexagonal.architecture.example.core.application;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import hexagonal.architecture.example.core.domain.Empresa;
import hexagonal.architecture.example.core.domain.Fecha;
import hexagonal.architecture.example.core.domain.Transferencia;
import hexagonal.architecture.example.core.repositories.EmpresaRepository;
import hexagonal.architecture.example.core.repositories.TransferenciaRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class ObtenerEmpresasConTransferenciasUltimoMes {

	private final TransferenciaRepository transferenciaRepository;
	private final EmpresaRepository empresaRepository;
	
	public List<Empresa> obtenerEmpresas() {
		var transferenciasUltimoMes = transferenciaRepository.obtenerTransferenciasDesde(Fecha.obtenerUnMesAtras());
		var empresasIds = transferenciasUltimoMes.stream().map(Transferencia::getIdEmpresa).collect(Collectors.toSet());
		return empresaRepository.obtenerEmpresas(empresasIds);
	}
}
