package hexagonal.architecture.example.core.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Empresa {
	
	private int cuit;
	private String razonSocial;
	private Fecha fechaAdhesion;
}
