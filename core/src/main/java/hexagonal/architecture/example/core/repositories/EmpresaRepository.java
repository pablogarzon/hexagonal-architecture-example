package hexagonal.architecture.example.core.repositories;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import hexagonal.architecture.example.core.domain.Empresa;

public interface EmpresaRepository {

	public List<Empresa> obtenerEmpresas(Collection<Integer> empresasIds);

	public List<Empresa> obtenerEmpresasAdheridasDesde(LocalDate localDate);

	public boolean existe(int cuit);
	
	public boolean crear(Empresa empresa);
}
