package hexagonal.architecture.example.core.application;

import java.util.List;

import org.springframework.stereotype.Service;

import hexagonal.architecture.example.core.domain.Empresa;
import hexagonal.architecture.example.core.domain.Fecha;
import hexagonal.architecture.example.core.repositories.EmpresaRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class ObtenerEmpresasAdheridasUltimoMes {
	
	private final EmpresaRepository empresaRepository;
	
	public List<Empresa> obtenerEmpresas() {
		return empresaRepository.obtenerEmpresasAdheridasDesde(Fecha.obtenerUnMesAtras());
	}
}
	