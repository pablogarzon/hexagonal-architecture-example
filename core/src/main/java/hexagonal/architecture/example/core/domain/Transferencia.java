package hexagonal.architecture.example.core.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Transferencia {

	private int id;
	private double importe;
	private int idEmpresa;
	private long cuentaDebito;
	private long cuentaCredito;
	private Fecha fecha;
}
