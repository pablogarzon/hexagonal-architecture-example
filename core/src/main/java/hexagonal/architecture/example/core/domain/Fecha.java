package hexagonal.architecture.example.core.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import lombok.Value;
/**
 * Value Object para manejar dates
 * @author Pablo Garzon
 *
 */
@Value
public class Fecha {
	private LocalDate date;
	
	private static final LocalDate ONEMONTHAGO_DATE = LocalDate.now().minusMonths(1);
	
	private static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyyy-MM-dd");
	
	public static Fecha crearFechaActual() {
		return new Fecha(LocalDate.now());
	}
	
	public static LocalDate obtenerUnMesAtras() {
		return ONEMONTHAGO_DATE;
	}
	
	public String getFormattedDate() {
	     return DTF.format(date);
	}
}
