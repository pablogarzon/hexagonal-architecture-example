package hexagonal.architecture.example.core.repositories;

import java.time.LocalDate;
import java.util.List;

import hexagonal.architecture.example.core.domain.Transferencia;

public interface TransferenciaRepository {
	public List<Transferencia> obtenerTransferenciasDesde(LocalDate from);
}
