package hexagonal.architecture.example.core.application;

import org.springframework.stereotype.Service;

import hexagonal.architecture.example.core.domain.Empresa;
import hexagonal.architecture.example.core.domain.Fecha;
import hexagonal.architecture.example.core.repositories.EmpresaRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class CrearEmpresa {

	private final EmpresaRepository empresaRepository;
	
	public boolean crear(Empresa empresa) {
		if(empresaRepository.existe(empresa.getCuit())) return false;
		empresa.setFechaAdhesion(Fecha.crearFechaActual());
		return empresaRepository.crear(empresa);
	}
}
